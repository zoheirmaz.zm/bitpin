from rest_framework.generics import ListAPIView

from timeline.models import Post
from timeline.serializers import PostSerializer


class PostViewSet(ListAPIView):
    queryset = Post.objects.all().order_by('-created_at')
    serializer_class = PostSerializer
