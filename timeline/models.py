from django.db import models


# Create your models here.


class Post(models.Model):
    title = models.TextField(
        "title",
        max_length=50,
        db_index=True
    )
    text = models.TextField(
        "text",
        max_length=250,
        db_index=False
    )

    score_count = models.PositiveIntegerField('score_count', null=True)
    score_average = models.FloatField('score_average', null=True,)

    created_at = models.DateTimeField(
        "Created at", auto_now_add=True, db_index=True, editable=False
    )

    # A timestamp representing when this object was last updated.
    updated_at = models.DateTimeField(
        "Updated at", auto_now=True, db_index=True, editable=False
    )
