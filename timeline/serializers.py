from rest_framework import serializers

from scoring.models import Score
from scoring.serializers import ScoreSerializer
from timeline.models import Post


class PostSerializer(serializers.ModelSerializer):
    user_score = serializers.SerializerMethodField()

    def get_user_score(self, obj):
        user_id = self.context["view"].request.query_params.get('user_id', None)
        user_score = Score.objects.filter(post_id=obj.id, user_id=user_id).first()
        return ScoreSerializer(user_score).data if user_score else None

    class Meta:
        model = Post
        fields = ['id', 'title', 'text', 'created_at', "score_count", "score_average", 'user_score']
