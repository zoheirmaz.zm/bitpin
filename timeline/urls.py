from django.urls import path

from timeline.views import PostViewSet

urlpatterns = [
    path('posts/', PostViewSet.as_view()),
]
