from django.urls import path

from scoring.views import ScoreViewSet

urlpatterns = [
    path('score/', ScoreViewSet.as_view()),
]
