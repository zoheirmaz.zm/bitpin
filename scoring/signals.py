from django.db.models import Avg, Count
from django.db.models.signals import post_save
from django.dispatch import receiver

from scoring.models import Score
from timeline.models import Post


@receiver(post_save, sender=Score)
def user_created(sender, instance, created, **kwargs):
    post_id = instance.post_id

    score_data = Score.objects.filter(post_id=post_id).aggregate(
        scores_count=Count('*'),
        avg_score=Avg('score')
    )

    post = Post.objects.get(pk=post_id)
    post.score_count = score_data['scores_count']
    post.score_average = score_data['avg_score']
    post.save()
