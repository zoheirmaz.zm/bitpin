from django.db import models


class Score(models.Model):
    post = models.ForeignKey(
        "timeline.Post",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name="post_scores",
        verbose_name="Post",
        db_index=True
    )
    score = models.PositiveSmallIntegerField(
        "Score", db_index=True
    )
    user_id = models.IntegerField('user_id', db_index=True)

    created_at = models.DateTimeField(
        "Created at", auto_now_add=True, db_index=True, editable=False
    )

    updated_at = models.DateTimeField(
        "Updated at", auto_now=True, db_index=True, editable=False
    )
