from rest_framework import serializers

from scoring.models import Score
from timeline.models import Post


class ScoreSerializer(serializers.ModelSerializer):
    post_id = serializers.IntegerField(write_only=True)
    score = serializers.IntegerField(min_value=0, max_value=5)

    def create(self, validated_data):
        try:
            post = Post.objects.get(pk=validated_data['post_id'])
        except Exception:
            raise serializers.ValidationError({'post_id': ["The value in invalid."]})
        score, _ = Score.objects.update_or_create(
            post=post,
            user_id=validated_data.get('user_id', None),
            defaults={'score': validated_data.get('score', None)}
        )
        return score

    class Meta:
        model = Score
        fields = ['id', 'post', 'post_id', 'score', 'user_id', 'created_at']
