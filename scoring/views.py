from rest_framework.generics import CreateAPIView

from scoring.models import Score
from scoring.serializers import ScoreSerializer


class ScoreViewSet(CreateAPIView):
    queryset = Score.objects.all()
    serializer_class = ScoreSerializer
